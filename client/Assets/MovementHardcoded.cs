using UnityEngine;
public class MovementHardcoded : Movement
{
	int index = 0;
	int[,] path;
	Vector3 offset;

	public MovementHardcoded(int [,] path_) {
		path = path_;
		offset = Get(0);
		index = 0;
	}

	public Vector3 Get(int i) {
		return new Vector3((float)path[index,0], 0, (float)path[index,1]) * 0.1f;
	}
	
	public void Move(Critter critter) {
		float left = critter.speed;
		critter.speed = critter.maxSpeed;

		Vector3 movement;

		while (left > 0f) {
			if (offset.magnitude < left) {
				movement = offset;
				left -= movement.magnitude;

				index = (index + 1) % path.GetLength(0);
				offset = Get (index);
			} else {
				movement = offset.normalized * left;
				offset -= movement;
				left = 0f;
			}

			critter.position += movement;
			critter.direction = movement.normalized;
		}
	}
}
