﻿Shader "Rim Light Specular With Alpha" {
	Properties {
		_MainTex ("Texture", 2D) = "white" {}
		_Cube ("Cubemap", CUBE) = "" {}
		_ReflColor ("Relection Color", Color) = (0.26,0.19,0.16,0.0)
			_RimColor ("Rim Color", Color) = (0.26,0.19,0.16,0.0)
			_RimPower ("Rim Power", Range(0.5,8.0)) = 3.0

	}
	SubShader {
		Tags { "RenderType" = "Opaque" }

		CGPROGRAM
#pragma surface surf Lambert
			struct Input {
				float2 uv_MainTex;
				float3 worldRefl;
				float3 viewDir;
				INTERNAL_DATA
			};
		sampler2D _MainTex;
		samplerCUBE _Cube;
		float4 _RimColor;
		float4 _ReflColor;
		float _RimPower;
		float _AlphPower;
		float _AlphaMin;

		half4 LightingLambert(SurfaceOutput s, half3 lightDir, half3 viewDir, half atten) {
			half3 h = normalize (lightDir + viewDir);

			half diff = max (0, dot (s.Normal, lightDir));

			float nh = max (0, dot (s.Normal, h));
			float spec = pow (nh, 12.0);

			half4 c;
			c.rgb = (s.Albedo * _LightColor0.rgb * diff + _LightColor0.rgb * spec) * (atten * 2);
			c.a = s.Alpha;
			return c;
		}

		void surf (Input IN, inout SurfaceOutput o) {
			o.Albedo = float4(0,0,0,1);
			half rim = 1.0 - saturate(dot (normalize(IN.viewDir), o.Normal));
			o.Emission = _RimColor.rgb * pow (rim, _RimPower) + texCUBE(_Cube, WorldReflectionVector (IN, o.Normal)).rgb*_ReflColor*2 ;
			o.Alpha = 1;
		}
		ENDCG
	}
	Fallback "VertexLit"
}
