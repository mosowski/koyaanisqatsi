﻿using UnityEngine;
using System.Collections;

public class WelderFx : MonoBehaviour {
	public GameObject smoke;
	public GameObject sparks;

	public void Destroy() {
		SetEmmisionEnabled(false);
		GameObject.DestroyObject(gameObject, 4);
	}

	public void SetEmmisionEnabled(bool v) {
		if (smoke != null) {
			smoke.particleSystem.enableEmission = v;
		}
		if (sparks != null) {
			sparks.particleSystem.enableEmission = v;
		}
	}
}
