﻿using UnityEngine;
using System.Collections;

public class TerrainTile : MonoBehaviour {

	public GameObject prefab;
	public float xSize = 50.54f;
	public float zSize = 48.09f;

	GameObject[,] tiles;
	Vector3 playerPosition;

	void Start () {
		Knot.terrainTile = this;

		tiles = new GameObject[3,3];
		for (int i = 0; i < 3; ++i) {
			for (int j = 0; j < 3; ++j) {
				tiles[i,j] = Instantiate(prefab) as GameObject;
			}
		}
	}

	public void SetPlayerPosition(Vector3 playerPosition) {
		this.playerPosition = playerPosition;
	}

	public bool Raycast(Ray ray, out RaycastHit hit) {
		for (int i = 0; i < 3; ++i) {
			for (int j = 0; j < 3; ++j) {
				if (tiles[i,j].collider.Raycast(ray, out hit, Mathf.Infinity)) {
					return true;
				}
			}
		}
		hit = new RaycastHit();
		return false;
	}
	
	void Update () {
		float cx = xSize * Mathf.Floor(playerPosition.x / xSize);
		float cz = zSize * Mathf.Floor(playerPosition.z / zSize);

		for (int i = 0; i < 3; ++i) {
			for (int j = 0; j < 3; ++j) {
				tiles[i,j].transform.localPosition = new Vector3(i * xSize + cx, 0, j * zSize + cz);
			}
		}
	}
}
