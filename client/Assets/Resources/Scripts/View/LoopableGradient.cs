﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class LoopableGradient {
	public Gradient gradient;
	public float length;
	public bool loop;

	public Color Evaluate(float p, float totalLength) {
		if (loop) {
			return gradient.Evaluate((p % length) / length);
		} else {
			return gradient.Evaluate(p / totalLength);
		}
	}
}
