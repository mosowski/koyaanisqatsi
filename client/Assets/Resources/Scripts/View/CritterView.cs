﻿using UnityEngine;
using System.Collections;

public class CritterView : MonoBehaviour {

	public bool hasTurnAnimation = false;
	public bool hasMoveAnimation = true;
	public bool hasAttackAnimation = false;

	public Critter critter;
	public Animator animator;
	public Vector3 targetRotation;
	public Vector3 currentRotation;
	public Vector3 targetPosition;
	public Vector3 currentPosition;
	
	public virtual void SetCritter(Critter critter) {
		this.critter = critter;
		targetRotation = currentRotation = Vector3.forward;
	}

	public virtual void SetPositionImmediately() {
		targetPosition = currentPosition = critter.position;
	}

	public virtual void Tick() {
		Ray groundRay = new Ray(critter.position + new Vector3(0,5,0), new Vector3(0,-1,0));
		RaycastHit hit;
		if (Knot.terrainTile.Raycast(groundRay, out hit)) {
			targetPosition = hit.point + new Vector3(0,critter.yOffset,0);
		} else {
			targetPosition = critter.position + new Vector3(0,critter.yOffset,0);
		}

		if (critter.attack == null) {
			targetRotation = critter.direction;
		}
		if (hasAttackAnimation) {
			animator.SetBool("IsAttacking", critter.attack != null);
		}

		animator.SetBool("IsAlive", critter.isAlive);
		if (!critter.isAlive) {
			Knot.pool.Release(gameObject, 2);
		}

		if (hasMoveAnimation) {
			animator.SetBool("IsMoving", critter.IsMoving);
		}
	}

	public virtual void Destroy() {
		GameObject.Destroy(gameObject);
	}

	public virtual void Update() {
		currentPosition += (targetPosition - currentPosition) * 12f * Time.deltaTime;
		transform.position = currentPosition;

		float turnSpeed = critter.attack == null ? 6f : 20f;
		targetRotation.y = 0;
		targetRotation.Normalize();
		currentRotation.Normalize();
		currentRotation += (targetRotation - currentRotation) * turnSpeed * Time.deltaTime;
		currentRotation.Normalize();

		if (critter.IsMoving) {
			transform.rotation =  Quaternion.FromToRotation(Vector3.forward, currentRotation);
		}

		if (!critter.isAlive) {
			targetPosition += new Vector3(0, -0.5f * Time.deltaTime, 0);
		}
	}
}
