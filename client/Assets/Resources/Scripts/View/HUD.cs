using UnityEngine;
using System.Collections;

public class HUD : MonoBehaviour {
	public static int kills = 0;
	public static int startup;
	public static int hp;
	void OnGUI () {
		int elapsed = (int) Time.time - startup;
		int minutes = elapsed / 60;
		int seconds = elapsed % 60;
		string formated_minutes = minutes.ToString();
		string formated_seconds = seconds.ToString();
		if (formated_minutes.Length == 1)
			formated_minutes = "0" + formated_minutes;
		if (formated_seconds.Length == 1)
			formated_seconds = "0" + formated_seconds;

		GUIStyle style = GUI.skin.GetStyle ("label");
		style.fontSize = 40;
		int size = 45;
		GUI.Label (new Rect (10,10,400,50), "Time: " + formated_minutes + ":" + formated_seconds);
		GUI.Label (new Rect (10,10+size,200,50), "Kills: "+kills);
		GUI.Label (new Rect (10,10+size+size,200,50), "HP: "+ ((int) Knot.world.Player.hp).ToString ());
	}
}