﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class LoopableCurve {
	public AnimationCurve curve;
	public float length;
	public bool loop;

	public float Evaluate(float p, float totalLength) {
		if (loop) {
			return curve.Evaluate((p % length) / length);
		} else {
			return curve.Evaluate(p / totalLength);
		}
	}
}
