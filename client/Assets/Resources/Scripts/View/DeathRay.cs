﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class DeathRay : MonoBehaviour {
	public Vector3 planeNormal;
	public GameObject start;
	public GameObject dest;

	public LoopableCurve sizeCurve;

	public LoopableCurve offsetCurve;
	public float noiseOffset;

	public LoopableCurve scaleCurve;
	public float noiseMinSize;
	public float noiseMaxSize;

	public float uSpeed = 0;
	public float uScale = 1;
	public bool uStretch = true;
	public bool uModuloLength = false;

	public int numSubNodes = 0;
	public float noiseFreq = 0;

	public Material material;
	public LoopableGradient gradient;

	Mesh mesh;
	MeshRenderer meshRenderer;
	MeshFilter meshFilter;

	List<float> noiseOffsets;
	List<float> noiseScales; 
	float lastJitterTime = 0;

	int numVertices;
	Vector3[] vertices;
	Vector2[] uvs;
	Color32[] colors;
	int[] indices;

	void Start () {
		meshRenderer = gameObject.GetComponent<MeshRenderer>();
		if (meshRenderer == null) {
			meshRenderer = gameObject.AddComponent<MeshRenderer>();
		}
		meshRenderer.material = material;
		meshFilter = gameObject.GetComponent<MeshFilter>();
		if (meshFilter == null) {
			meshFilter = gameObject.AddComponent<MeshFilter>();
		}
		meshFilter.mesh = mesh = new Mesh();
		noiseOffsets = new List<float>();
		noiseScales  = new List<float>();
		vertices = new Vector3[0];
		ResizeNoiseArrays();
	}

	void ResizeNoiseArrays() {
		if (noiseOffsets.Count != numSubNodes) {
			while (noiseOffsets.Count < numSubNodes) {
				noiseOffsets.Add(0);
				noiseScales.Add(1);
			} 

			if (noiseOffsets.Count > numSubNodes) {
				noiseOffsets.RemoveRange(numSubNodes, noiseOffsets.Count - numSubNodes);
				noiseScales.RemoveRange(numSubNodes, noiseScales.Count - numSubNodes);
			}
		}
	}

	void UpdateNoise() {
		if ((noiseOffset > 0 || noiseMaxSize != 1 || noiseMinSize != 1) 
			&& Time.time - lastJitterTime > 1.0f/noiseFreq) {

			ResizeNoiseArrays();

			for (int i = 0; i < numSubNodes; ++i) {
				noiseOffsets[i] = 0;
				noiseScales[i] = 1;
			}

			int r = numSubNodes / 2;
			float o = noiseOffset;
			float sMin = noiseMinSize - 1;
			float sMax = noiseMaxSize - 1;
			while (r > 0) {
				float o0 = 0;
				float s0 = 0;
				for (int i = 0; i < numSubNodes; i += r) {
					float o1 = (i + r) >= numSubNodes ? 0 : o * (Random.value - 0.5f);
					float s1 = (i + r) >= numSubNodes ? 0 : Random.Range(sMin, sMax);
					for (int j = i; j < i + r && j < numSubNodes; ++j) {
						noiseOffsets[j] += o0 + (o1 - o0) * (j - i + 1) / r;
						noiseScales[j] += s0 + (s1 - s0) * (j - i + 1) / r;
					}
					o0 = o1;
					s0 = s1;
				}
				r /= 2;
				o /= 2;
				sMin /= 2;
				sMax /= 2;
			}

			for (int i = 0; i < numSubNodes; ++i) {
				noiseOffsets[i] *= offsetCurve.Evaluate(i, numSubNodes);
				noiseScales[i] = 1.0f + (noiseScales[i] - 1) * scaleCurve.Evaluate(i, numSubNodes);
			}

			lastJitterTime = Time.time;
		}
		UpdateMesh();
	}

	void UpdateMesh() {
		if (start == null || dest == null) {
			return;
		}

		numVertices = (numSubNodes + 2) * 2;
		if (vertices.Length != numVertices) {
			vertices = new Vector3[numVertices];
			uvs = new Vector2[numVertices];
			colors = new Color32[numVertices];

			indices = new int[(numSubNodes + 1) * 4];

			for (int i = 0; i < numSubNodes + 1; ++i) {
				indices[i * 4 + 0] = i * 2 + 0;
				indices[i * 4 + 1] = i * 2 + 2;
				indices[i * 4 + 2] = i * 2 + 3;
				indices[i * 4 + 3] = i * 2 + 1;
			}
		}

		Vector3 localStart = transform.InverseTransformPoint(start.transform.position);
		Vector3 localDest = transform.InverseTransformPoint(dest.transform.position);
		Vector3 direction = localDest - localStart;
		Vector3 side = Vector3.Cross(direction, planeNormal).normalized;
		float totalLength = (localDest - localStart).magnitude;
		if (totalLength == 0) {
			// nothing to do here!
			return; 
		}

		float uStart = uSpeed * Time.time;
		float uDest = 0;
		float uLen;
		if (uStretch) {
			uDest = uSpeed * Time.time + uScale;
			uLen = uScale;
		} else {
			uDest = uSpeed * Time.time + uScale * totalLength;
			uLen = uScale * totalLength;
		}
		if (uModuloLength) {
			renderer.material.SetVector("_Modulo", new Vector4(uLen, 1));
		}
		float startSize = sizeCurve.Evaluate(0, totalLength);
		float destSize = sizeCurve.Evaluate(1, totalLength);

		vertices[0] = localStart + side * startSize;
		vertices[1] = localStart - side * startSize;
		colors[0] = colors[1] = gradient.Evaluate(0, totalLength);
		uvs[0] = new Vector2(uStart, 0);
		uvs[1] = new Vector2(uStart, 1);



		for (int i = 1; i <= numSubNodes; ++i) {
			Vector3 p = localStart + (localDest - localStart) * i / numSubNodes;

			float s = sizeCurve.Evaluate((p - localStart).magnitude, totalLength);

			vertices[i * 2 + 0] = p + noiseOffsets[i-1] * side + side * s * noiseScales[i-1];
			vertices[i * 2 + 1] = p + noiseOffsets[i-1] * side - side * s * noiseScales[i-1];

			colors[i * 2 + 0] = colors[i * 2 + 1] = gradient.Evaluate((p - localStart).magnitude, totalLength);
			uvs[i * 2 + 0] = new Vector2(uStart + (uDest - uStart) * i / numSubNodes, 0);
			uvs[i * 2 + 1] = new Vector2(uStart + (uDest - uStart) * i / numSubNodes, 1);
		}

		vertices[numVertices - 2] = localDest + side * destSize;
		vertices[numVertices - 1] = localDest - side * destSize;
		colors[numVertices - 2] = colors[numVertices - 1] = gradient.Evaluate(1, 1);
		uvs[numVertices - 2] = new Vector2(uDest, 0);
		uvs[numVertices - 1] = new Vector2(uDest, 1);

		mesh.Clear();
		mesh.vertices = vertices;
		mesh.uv = uvs;
		mesh.colors32 = colors;
		mesh.SetIndices(indices, MeshTopology.Quads, 0);
		mesh.RecalculateBounds();

	}
	
	void Update () {
		UpdateNoise();
	}
}
