﻿using UnityEngine;
using System.Collections;

public class LazorAttackView : MonoBehaviour {
	public LazorAttack lazorAttack;

	public GameObject attackSource;
	public GameObject lazorStart;
	public GameObject lazorEnd;

	public GameObject welderFx;
	public GameObject terrainWelderFx;

	Vector3 currentAttackPoint;

	public void Init(LazorAttack lazorAttack) {
		this.lazorAttack = lazorAttack;
		lazorEnd.transform.localPosition = lazorStart.transform.localPosition = Vector3.zero;
		currentAttackPoint = lazorAttack.attackPoint;

		attackSource = lazorAttack.critter.view.gameObject.FindRecursiveForce("AttackSource");

		welderFx = Instantiate(Resources.Load<GameObject>("Attacks/Fx/Welder")) as GameObject;
		welderFx.transform.parent = lazorEnd.transform;
		welderFx.transform.localPosition = Vector3.zero;

		terrainWelderFx = Instantiate(Resources.Load<GameObject>("Attacks/Fx/SmallWelder")) as GameObject;
		terrainWelderFx.transform.parent = null;
		terrainWelderFx.GetComponent<WelderFx>().SetEmmisionEnabled(false);
	}

	void Update() {
		if (lazorAttack == null) {
			return;
		}

		if (lazorAttack.HasCompleted()) {
			welderFx.transform.parent = null;
			welderFx.GetComponent<WelderFx>().Destroy();
			terrainWelderFx.GetComponent<WelderFx>().Destroy();
			GameObject.Destroy(gameObject);
		} else {
			Vector3 critterPos = lazorAttack.critter.view.transform.position;
			currentAttackPoint += (lazorAttack.attackPoint - currentAttackPoint) * 6f * Time.deltaTime;

			Vector3 raySource;
			if (attackSource != null) {
				raySource = attackSource.transform.position;
			} else {
				raySource = critterPos + new Vector3(0,0.6f,0);
			}

			lazorStart.transform.position = raySource;
			lazorEnd.transform.position = currentAttackPoint;

			Ray ray = new Ray(raySource, (currentAttackPoint - raySource).normalized);
			RaycastHit hit;
			if (Knot.terrainTile.Raycast(ray, out hit)) {
				terrainWelderFx.GetComponent<WelderFx>().SetEmmisionEnabled(true);
				terrainWelderFx.transform.position = hit.point;
			} else {
				terrainWelderFx.GetComponent<WelderFx>().SetEmmisionEnabled(false);
			}


			// to jest problem bo to widok crittera powinien kontrolowac
			lazorAttack.critter.view.targetRotation = (currentAttackPoint - critterPos).normalized;
		}
	}
}
