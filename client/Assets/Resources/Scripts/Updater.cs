﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Updater : MonoBehaviour {

	public Camera gameCamera;
	public Vector3 cameraOffset = new Vector3(-6, 10, -6);
	Vector3 currentCameraPosition;
	Vector3 currentCameraTargetPos;

	Critter mainCritter;
	float lastTickTime;
	Plane groundPlane;
	Vector3 worldTouchStartPosition;
	Vector3 screenTouchStartPosition;
	bool isSwipingAttack;

	List<Vector3> swipe;


	void Start() {
		groundPlane = new Plane(new Vector3(0,1,0), Vector3.zero);
		Knot.playerId = "Joe";
		Knot.Init(this);
		Knot.world.SetPlayerId(Knot.playerId);
		Application.targetFrameRate = 60;
	}

	public void SetMainCritter(Critter mainCritter) {
		this.mainCritter = mainCritter;
		Quaternion angle = Quaternion.Euler(55, 60, 0);
		gameCamera.transform.rotation = angle;
	}

	void UpdateCamera() {
		currentCameraTargetPos += (mainCritter.view.transform.position - currentCameraTargetPos) * 12f * Time.deltaTime;
		currentCameraPosition += (currentCameraTargetPos + cameraOffset - currentCameraPosition) * 6f * Time.deltaTime;
		gameCamera.transform.localPosition = currentCameraPosition;
		gameCamera.transform.LookAt(currentCameraTargetPos);
	}
	
	void Update() {
		UpdateInput();

		if (Time.time - lastTickTime > Config.TickLength) {
			lastTickTime = Time.time;

			Knot.world.UpdateTick();
		}

		UpdateCamera();
		UpdateTerrain();

		Knot.pool.Update();
	}

	void UpdateTerrain() {
		if (Knot.terrainTile != null) {
			Knot.terrainTile.SetPlayerPosition(Knot.world.Player.view.transform.position);
		}
	}

	void UpdateInputState(Vector3 pos, bool isUp, bool isDown) {
		Ray ray = gameCamera.ScreenPointToRay(pos);
		float distance = 0;
		RaycastHit hit;
		Knot.terrainTile.Raycast(ray, out hit);

		Vector3 worldClickPos = hit.point;

		if (isUp) {
			if (!isSwipingAttack) {
				Knot.world.QueueCommand(new MoveCommand() { playerId = Knot.playerId, target = worldTouchStartPosition });
			} else {
				Knot.world.QueueCommand(new AttackCommand() { playerId = Knot.playerId, swipe = new List<Vector3>(swipe) });
			}
			isSwipingAttack = false;
			swipe = null;
		}

		if (swipe != null && swipe.Count >= 1 && Vector3.Distance (screenTouchStartPosition, pos) >= 5) {
			isSwipingAttack = true;
		}

		if (isDown) {
			screenTouchStartPosition = pos;
			worldTouchStartPosition = worldClickPos;

			swipe = new List<Vector3>();
			isSwipingAttack = false;
		}


		if (swipe != null) {
	
			if (swipe.Count == 0 || Vector3.Distance(swipe[swipe.Count - 1], worldClickPos) > 1) {
				swipe.Add(worldClickPos);
			}
		}

	}

	void UpdateInput() {
		Vector3 pos = Vector3.zero;
		bool isDown = false;
		bool isUp = false;

		if (Application.isMobilePlatform) {
			for (var i = 0; i < Input.touchCount; ++i) {
				pos = Input.GetTouch(i).position;
				if (Input.GetTouch(i).phase == TouchPhase.Began) {
					isDown = true;
				} else if (Input.GetTouch(i).phase == TouchPhase.Ended) {
					isUp = true;
				}
				break;
			}
		} else {
			pos = Input.mousePosition;
			isDown = Input.GetMouseButtonDown(0);
			isUp = Input.GetMouseButtonUp(0);
		}
		UpdateInputState(pos, isUp, isDown);
	}
}
