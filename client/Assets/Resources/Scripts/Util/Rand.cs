using UnityEngine;
using System;
public class Rand
{
	private static System.Random randomness = new System.Random();

	public static int RandomInt(int from, int to)
	{
		return randomness.Next(from, to);
	}

	public static double RandomDouble()
	{
		return randomness.NextDouble();
	}

	public static float RandomFloat()
	{
		return (float) randomness.NextDouble();
	}

	public static float RandomFloat(float from, float to)
	{
		return from + (to - from) * (float) randomness.NextDouble();
	}

	public static Vector3 RandomPointOnCircle(Vector3 center, float radius) {
		float a = RandomFloat() * Mathf.PI * 2;
		return center + new Vector3(Mathf.Sin(a) * radius, 0, Mathf.Cos(a) * radius);
	}
}

