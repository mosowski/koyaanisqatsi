﻿using UnityEngine;
using System;
using System.Collections;

public static class Helpers {
	public static GameObject FindAncestor(this GameObject go, string name) {
		Transform t = go.transform;
		while (t != null) {
			if (t.gameObject.name == name) {
				return t.gameObject;
			}
			t = t.parent;
		}
		return null;
	}
	
	public static GameObject FindRecursiveForce(this GameObject go, string name) {
		if (go.name == name) {
			return go;
		}

		Transform t = go.transform.Find(name);
		if (t != null) {
			return t.gameObject;
		} else {
			int c = go.transform.childCount;
			for(int i = 0;i<c;i++) {
				GameObject tmp = go.transform.GetChild(i).gameObject.FindRecursiveForce(name);
				if (tmp != null) {
					return tmp;
				}
			}
			return null;
		}
	}

	public static GameObject FindRecursive(this GameObject go, string name, bool safe = true) {
		foreach (Transform t in go.GetComponentsInChildren<Transform>()) {
			if (t.gameObject.name == name) {
				return t.gameObject;
			}
		}
		if (safe) {
			throw new Exception("Whoa! Child " + name + " not found in " + go.name);
		} else {
			return null;
		}
	}

	public static GameObject FindChild(this GameObject go, string name, bool safe = true) {
		Transform t = go.transform.Find(name);
		if (t != null) {
			return t.gameObject;
		} else if (safe) {
			throw new Exception("Whoa! Child " + name + " not found in " + go.name);
		} else {
			return null;
		}
	}

	public static T FindComponentRecursive<T>(this GameObject go, string name, bool safe = true) where T : Component {
		return go.FindRecursive(name, safe).GetComponent<T>();
	}

}

