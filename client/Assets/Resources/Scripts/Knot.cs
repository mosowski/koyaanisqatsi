﻿using UnityEngine;
using System.Collections;

public class Knot : MonoBehaviour {
	public static Updater updater;
	public static World world;
	public static string playerId;
	public static TerrainTile terrainTile;
	public static GameObjectPool pool;

	public static void Init(Updater updater) {
		pool = new GameObjectPool();

		Knot.updater = updater;

		world = new World();
		world.Init();

		PlayerCritter playerCritter = new PlayerCritter();
		playerCritter.Init();
		playerCritter.id = Knot.playerId;
		GameObject view = Instantiate(Resources.Load<GameObject>("Critters/SkyShark")) as GameObject;
		playerCritter.SetGameObject(view);
		playerCritter.movement = new MovementSharking();
		playerCritter.yOffset = 1;
		playerCritter.position = new Vector3 (10, 0, 10);
		playerCritter.moveDestination = new Vector3 (10, 0, 10);

		world.AddCritter(playerCritter);

		/*Critter critter = new Critter();
		critter.Init();
		critter.id = "Hindus.1";
		view = Instantiate(Resources.Load<GameObject>("Critters/Hindus")) as GameObject;
		critter.SetGameObject(view);
		world.AddCritter(critter);
*/

		updater.SetMainCritter(playerCritter);

		GameObject gui = new GameObject ();

		HUD.startup = (int) Time.time;

		gui.AddComponent ("HUD");
	}


}
