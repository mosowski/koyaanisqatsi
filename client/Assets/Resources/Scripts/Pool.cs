﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameObjectPool {
	public class _GameObjectPool : Pool {
		public override GameObject Construct(GameObject prefab) {
			GameObject obj = Object.Instantiate(prefab) as GameObject;
			return obj;
		}
	}

	_GameObjectPool pool;

	class ReleaseJob {
		public GameObject gameObject;
		public float time;
	}

	List<ReleaseJob> releaseJobs;

	public GameObjectPool() {
		pool = new _GameObjectPool();
		releaseJobs = new List<ReleaseJob>();
	}

	public GameObject Pick(string id) {
		return Pick(Resources.Load<GameObject>(id));
	}

	public GameObject Pick(GameObject prefab) {
		GameObject obj = pool.Pick(prefab);
		obj.transform.localScale = Vector3.one;
		obj.transform.localRotation = Quaternion.identity;
		obj.SendMessage("OnPicked", SendMessageOptions.DontRequireReceiver);
		return obj;
	}

	public void Release(GameObject obj, float timeDelay) {
		if (timeDelay == 0) {
			Release(obj);
		} else {
			ReleaseJob rj = new ReleaseJob();
			rj.gameObject = obj;
			rj.time = Time.time + timeDelay;
			releaseJobs.Add(rj);
		}
	}
	public void Release(GameObject obj) {
		if (obj != null) {
			obj.SendMessage("OnReleased", SendMessageOptions.DontRequireReceiver);
			pool.Release(obj);
		}
	}

	public void Update() {
		for (int i = releaseJobs.Count - 1; i >= 0; --i) {
			if (Time.time >= releaseJobs[i].time) {
				if (releaseJobs[i].gameObject != null) {
					Release(releaseJobs[i].gameObject);
				}
				releaseJobs.RemoveAt(i);
			}
		}
	}
}

public class Pool {
	Dictionary<string, Stack<GameObject>> pools = new Dictionary<string, Stack<GameObject>>();
	Dictionary<GameObject, string> prefabsIds = new Dictionary<GameObject, string>();
	int uniqueId = 0;

	public virtual GameObject Construct(GameObject prefab) {
		return null;
	}

	string GetPrefabId(GameObject prefab) {
		if (!prefabsIds.ContainsKey(prefab)) {
			string id = (uniqueId++).ToString();
			prefabsIds[prefab] = id;
			return id;
		} else {
			return prefabsIds[prefab];
		}
	}

	public GameObject Pick(GameObject prefab) { 
		string prefabId = GetPrefabId(prefab);

		Stack<GameObject> pool;
		if (!pools.ContainsKey(prefabId)) {
			pool = pools[prefabId] = new Stack<GameObject>();
		} else {
			pool = pools[prefabId];
		}

		GameObject obj = null;

		if (pool.Count > 0) {
			obj = pool.Pop();
			obj.SetActive(true);
		} else {
			obj = Construct(prefab);
			obj.name = prefabId+"/" + prefab.name;
		}
		return obj;
	}

	public void Release(GameObject obj) {
		if (obj.activeSelf || obj.transform.parent != null) {
			string id = obj.name.Substring(0, obj.name.IndexOf("/"));
			if (pools.ContainsKey(id)) {
				pools[id].Push(obj);
				obj.transform.parent = null;
				obj.SetActive(false);
			}
		}
	}
}
