using UnityEngine;
using System.Collections;

public class Critter {

	public string id;
	public bool isAlive;
	public Vector3 position;
	public Vector3 direction;
	public Vector3 moveDestination;
	public float radius;
	public float speed;
	public float maxSpeed;
	public float yOffset = 0;
	public float hp;
	public bool fodder = false;

	public Movement movement;
	public Attack attack;

	public CritterView view;

	public Critter() {
		Init();
	}

	public virtual void KilledSomeone(Critter enemy) {
	}

	public virtual void Init() {
		isAlive = true;
		maxSpeed = 0.3f;
	}

	public virtual void SetGameObject(GameObject go) {
		view = go.GetComponent<CritterView>();
		if (view != null) {
			view.SetCritter(this);
		}
	}

	public virtual void SetPositionImmediately(Vector3 position) {
		this.position = position;
		view.SetPositionImmediately();
	}

	public virtual void Tick() {
		movement.Move(this);

		if (attack != null) {
			attack.Tick();
		}

		view.Tick();

		if (attack != null && attack.HasCompleted()) {
			attack = null;
		}
	}

	public virtual void SwitchToAttack(Attack newAttack) {
		if (attack != null) {
			attack.Cancel();
		}
		attack = newAttack;
	}

	public virtual void Destroy() {
		view.Destroy();
	}

	public virtual bool IsMoving {
		get {
			return speed != 0;
		}
	}

	public void Damage(float damage, Critter source = null) {
		hp -= damage;
		if (hp < 0) {
			isAlive = false;
		}
		if (source != null) {
			source.KilledSomeone(this);
		}
	}
}
