﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LazorAttack : Attack {

	public List<Vector3> attackSwipe;
	public float speed = 4;
	public float progress = 0;
	public Vector3 attackPoint;

	public PointAttackDamage attackDamage;

	GameObject view;

	public void Init(Critter critter, List<Vector3> attackSwipe) {
		base.Init(critter);
		this.attackSwipe = attackSwipe;

		attackPoint = attackSwipe[0];

		view = Object.Instantiate(Resources.Load<GameObject>("Attacks/LazorAttack")) as GameObject;
		view.GetComponent<LazorAttackView>().Init(this);
	}

	public override bool HasCompleted() {
		return ((int)progress) >= attackSwipe.Count - 1;
	}

	public override void Tick() {
		progress += 0.3f;
		progress = Mathf.Min(progress, attackSwipe.Count);

		int fromStep = (int)(progress);
		float factor = progress - fromStep;
		if (fromStep < attackSwipe.Count - 1) {
			attackPoint = attackSwipe[fromStep] * (1 - factor) + attackSwipe[fromStep + 1] * factor;
			attackDamage = new PointAttackDamage(attackPoint, 1, 10, critter);
			Knot.world.AddAttackDamage(attackDamage);
		}
	}

	public override void Cancel() {
		progress = (float)attackSwipe.Count;
	}

}
