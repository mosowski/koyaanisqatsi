﻿using UnityEngine;
using System.Collections;

public class MovementSharking : Movement {

	Vector3 currentTarget;
	bool reachedTarget = false;

	public MovementSharking() {
	}

	public void Move(Critter critter) {
		if (critter.moveDestination != currentTarget) {
			currentTarget = critter.moveDestination;
			reachedTarget = false;
		}

		if (!reachedTarget && Vector3.Distance(critter.position, critter.moveDestination) > critter.maxSpeed) {
			critter.direction = (critter.moveDestination - critter.position).normalized;
			critter.speed = critter.maxSpeed;
		} else {
			reachedTarget = true;
			critter.speed = critter.maxSpeed * (critter.attack == null ? 0.25f : 0.1f);
		}

		if (critter.speed != 0) {
			critter.position += critter.speed * critter.direction;
		}

	}
}


