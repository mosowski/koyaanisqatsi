
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StoneAttack : Attack {


	public Vector3 position;
	public Vector3 target;
	public float speed;
	
	public PointAttackDamage attackDamage;
	
	GameObject view;

	bool reachedTarget = false;
	
	public void Init(Critter critter, Vector3 target, Vector3 position,float speed) {
		base.Init(critter);

		this.target = target;
		this.position = position;

		this.speed = speed;

		view = Knot.pool.Pick("Critters/Hindus");
	}
	
	public override bool HasCompleted() {
		return reachedTarget;  
	}
	
	public override void Tick() {
		 
		if (Vector3.Distance(position, target) > speed) {
			Vector3 direction = (target - position).normalized;
			position += speed * direction;
			view.transform.position = position;
		} else {
			reachedTarget = true;
			position = target;
			Knot.pool.Release(view);
		}

		attackDamage = new PointAttackDamage(position, 1, 10, critter);
		Knot.world.AddAttackDamage(attackDamage);

	}
	
}