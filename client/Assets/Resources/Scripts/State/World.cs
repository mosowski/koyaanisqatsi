﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class World {

	int tick;
	public List<Critter> critters;
	Dictionary<string,Critter> critterById;
	string playerId;

	List<Command> queuedCommands;
	List<Command> remoteCommands;

	Director director;

	public int Tick {
		get { 
			return tick;
		}
	}

	public void Init() {
		critters = new List<Critter>();
		critterById = new Dictionary<string,Critter>();
		queuedCommands = new List<Command>();
		remoteCommands = new List<Command>();
		director = new Director ();
		director.Init ();
		tick = 0;
	}

	public void SetPlayerId(string playerId) {
		this.playerId = playerId;
	}

	public void QueueCommand(Command command) {
		command.tick = tick + 1;
		command.sender = playerId;

		//TODO: multi - push commands to queuedCommands
		remoteCommands.Add(command);
	}

	public void AddCritter(Critter critter) {
		critters.Add(critter);
		critterById[critter.id] = critter;
	}

	public void UpdateTick() {
		foreach (Command cmd in remoteCommands) {
			if (cmd.tick == tick) {
				RunCommand(cmd);
			}
		}
		remoteCommands.RemoveAll(cmd => cmd.tick == tick);

		foreach (Critter critter in critters) {
			critter.Tick();
		}

		critters.RemoveAll(critter => !critter.isAlive);

		director.Tick ();

		tick++;
	}

	public void RunCommand(Command cmd) {
		if (cmd is MoveCommand) {
			MoveCommand moveCommand = cmd as MoveCommand;
			PlayerCritter playerCritter = critterById[moveCommand.playerId] as PlayerCritter;
			playerCritter.moveDestination = moveCommand.target;
			playerCritter.moveDestination.y = 0;
		} else if (cmd is AttackCommand) {
			AttackCommand attackCommand = cmd as AttackCommand;
			PlayerCritter playerCritter = critterById[attackCommand.playerId] as PlayerCritter;
			LazorAttack lazorAttack = new LazorAttack();
			lazorAttack.Init(playerCritter, attackCommand.swipe);
			playerCritter.SwitchToAttack(lazorAttack);
		}
	}

	public Critter Player {
		get {
			return critterById[playerId];
		}
	}

	public void AddAttackDamage(AttackDamage damage) {
		if (damage is PointAttackDamage) {
			PointAttackDamage pointDamage = damage as PointAttackDamage;
			foreach (Critter critter in critters) {
				if (critter != pointDamage.attacker) {
					Vector3 dist = critter.position - pointDamage.position;
					dist.y = 0;
					if (dist.magnitude <= pointDamage.radious) {
						critter.Damage(pointDamage.damage, pointDamage.attacker);
					}
				}
			}
		}
		
	}
}
