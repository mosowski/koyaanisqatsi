﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Command {
	public string type;
	public string sender;
	public int tick;
}

public class MoveCommand : Command {
	public string playerId;
	public Vector3 target;
	
	public MoveCommand() {
		type = "MoveCommand";
	}
}

public class AttackCommand : Command {
	public string playerId;
	public List<Vector3> swipe;
	
	public AttackCommand() {
		type = "AttackCommand";
	}
}

