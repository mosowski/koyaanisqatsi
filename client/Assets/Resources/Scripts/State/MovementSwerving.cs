using UnityEngine;

public class MovementSwerving : Movement
{
	public int duration = 0;
	public int swerveDirection;
	public float swervePower;

	public float swerveMin;
	public float swerveMax;

	public MovementSwerving (float swerveMin_, float swerveMax_)
	{
		swerveMin = swerveMin_;
		swerveMax = swerveMax_;
	}

	public void Move(Critter critter)
	{
		if (duration == 0) {
			duration = Rand.RandomInt (3, 5);
			swerveDirection = Rand.RandomInt (0, 2) == 0 ? 1 : -1;
			swervePower = Rand.RandomFloat (swerveMin, swerveMax);
		} else {
			duration--;
		}
		Vector3 swerve = new Vector3(0, 0, 0);
		if (Vector3.Distance(critter.position, critter.moveDestination) > critter.maxSpeed) {
			critter.direction = (critter.moveDestination - critter.position).normalized;

			Vector3 perpendicular = new Vector3(swerveDirection * critter.direction.z, 0, -swerveDirection * critter.direction.x);
			swerve = perpendicular;

			critter.speed = critter.maxSpeed;
		} else {
			critter.speed = 0;
		}

		if (critter.speed != 0) {
			critter.position += critter.speed * (swervePower * swerve + (1 - swervePower) * critter.direction);
		}

	}
}


