using UnityEngine;
using System.Collections;

public class PointAttackDamage : AttackDamage {
	public Vector3 position;
	public float radious;
	public float damage;
	public Critter attacker;

	public PointAttackDamage(Vector3 position_, float radious_, float damage_, Critter attacker_) {
		Init(position_, radious_, damage_, attacker_);
	}

	public void Init(Vector3 position_, float radious_, float damage_, Critter attacker_) {
		position = position_;
		radious = radious_;
		damage = damage_;
		attacker = attacker_;
	}
}
