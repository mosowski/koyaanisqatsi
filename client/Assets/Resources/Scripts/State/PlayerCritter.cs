﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerCritter : Critter {

	public void Init() {
		hp = 100f;
		base.Init();
	}
	public override void Tick() {
		base.Tick();
	}
	public override void KilledSomeone(Critter critter) {
		HUD.kills++;
	}
}
