﻿using UnityEngine;
using System.Collections;

public class Attack {

	public Critter critter;

	public virtual void Init(Critter critter) {
		this.critter = critter;
	}

	public virtual bool HasCompleted() {
		return true;
	}
	
	public virtual void Tick() {
	}

	public virtual void Cancel() {
	}
}
