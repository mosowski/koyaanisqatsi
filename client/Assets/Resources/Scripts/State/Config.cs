﻿using UnityEngine;
using System.Collections;

public class Config {

	public static readonly int TurnLength = 16;
	public static readonly float TickLength = 0.03f;

}
